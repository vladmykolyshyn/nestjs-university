import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  ValidationPipe,
  HttpCode,
  Query,
  ParseIntPipe,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { StudentsService } from '../../services/students/students.service';
import { CreateStudentDto } from '../../services/students/dto/create-student.dto';
import { UpdateStudentDto } from '../../services/students/dto/update-student.dto';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';
import {
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { CreateStudentResponseDto } from 'src/services/students/dto/create-student-response.dto';
import { AddGroupToStudentDto } from 'src/services/students/dto/add-group-to-student.dto';
import { GetStudentResponseDto } from 'src/services/students/dto/get-student-response.dto';
import { GetStudentCoursesGroupsResponseDto } from 'src/services/students/dto/get-student-courses-groups-response.dto';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Controller('students')
@ApiTags('Students')
@ApiSecurity('JWT-auth')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @ApiOperation({ summary: 'Get all students' })
  @ApiQuery({
    name: 'name',
    description: 'Name of student',
    required: false,
    type: String,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The list of students',
    type: GetStudentResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getAllStudents(@Query('name') name: string) {
    if (name) {
      return this.studentsService.getStudentsByName(name);
    } else {
      return this.studentsService.getAllStudents();
    }
  }

  @ApiOperation({ summary: 'Get all students with groups and courses' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The list of students',
    type: GetStudentCoursesGroupsResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get('groups')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getAllStudentsWithGroupsAndCourses(@Query() queryFilter: QueryFilterDto) {
    return this.studentsService.getAllStudentsWithGroupsAndCourses(queryFilter);
  }

  @ApiOperation({ summary: 'Get student by id' })
  @ApiParam({
    name: 'id',
    description: 'Student id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The student by id',
    type: GetStudentResponseDto,
    isArray: false,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getStudentById(@Param('id', ParseIntPipe) id: number) {
    return this.studentsService.getStudentById(+id);
  }

  @ApiOperation({ summary: 'Create student' })
  @ApiBody({ type: CreateStudentDto })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Return the student after creation',
    type: CreateStudentResponseDto,
    isArray: false,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed or student with this email already exist',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Post()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.CREATED)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  createStudent(@Body() createStudentDto: CreateStudentDto) {
    return this.studentsService.createStudent(createStudentDto);
  }

  @ApiOperation({ summary: 'Update student' })
  @ApiParam({
    name: 'id',
    description: 'Student id',
    required: true,
    type: Number,
  })
  @ApiBody({ type: UpdateStudentDto })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Student is updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student or group not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Patch(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  updateStudentById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateStudentDto: UpdateStudentDto,
  ) {
    return this.studentsService.updateStudentById(+id, updateStudentDto);
  }

  @ApiOperation({ summary: 'Add group to student' })
  @ApiParam({
    name: 'id',
    description: 'Student id',
    required: true,
    type: Number,
  })
  @ApiBody({ type: AddGroupToStudentDto })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Group is added',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student or group not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Patch(':id/group')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  addGroupToStudent(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateStudentDto: AddGroupToStudentDto,
  ) {
    return this.studentsService.addGroupToStudent(+id, updateStudentDto);
  }

  @ApiOperation({ summary: 'Delete student by id' })
  @ApiParam({
    name: 'id',
    description: 'Student id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Student is deleted',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Delete(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  deleteStudentById(@Param('id', ParseIntPipe) id: number) {
    return this.studentsService.deleteStudentById(+id);
  }
}
