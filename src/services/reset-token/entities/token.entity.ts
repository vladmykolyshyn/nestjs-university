
import { CoreEntity } from '../../../application/entities/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

@Entity({ name: 'tokens' })
export class Token extends CoreEntity {
  @Column({
    type: 'integer',
    nullable: false,
  })
  userId: number;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  token: string;

  @OneToOne(() => Lector, (lector) => lector.token)
  @JoinColumn({ name: 'userId' })
  lector: Lector;
}
