import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { Group } from './entities/group.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { GetGroupResponseDto } from './dto/get-group-response.dto';
import { CreateGroupResponseDto } from './dto/create-group-response.dto';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private readonly groupsRepository: Repository<Group>,
  ) {}

  async getAllGroups(): Promise<GetGroupResponseDto[]> {
    const groups = await this.groupsRepository
      .createQueryBuilder('group')
      .leftJoinAndSelect('group.students', 'students')
      .getMany();
    return groups;
  }

  async getGroupById(id: number): Promise<GetGroupResponseDto> {
    const group = await this.groupsRepository
      .createQueryBuilder('group')
      .leftJoinAndSelect('group.students', 'students')
      .where('group.id = :id', { id })
      .getOne();

    if (!group) {
      throw new NotFoundException('Group not found');
    }

    return group;
  }

  async createGroup(
    groupCreateSchema: CreateGroupDto,
  ): Promise<CreateGroupResponseDto> {
    const group = await this.groupsRepository.findOne({
      where: {
        name: groupCreateSchema.name,
      },
    });

    if (group) {
      throw new BadRequestException('Group with this name already exists');
    }

    return this.groupsRepository.save(groupCreateSchema);
  }

  async updateGroupById(
    id: number,
    groupUpdateSchema: UpdateGroupDto,
  ): Promise<UpdateResult> {
    const group = await this.groupsRepository.findOne({
      where: { id },
    });

    if (!group) {
      throw new NotFoundException('Group not found');
    }

    return await this.groupsRepository.update(id, groupUpdateSchema);
  }

  async deleteGroupById(id: number): Promise<DeleteResult> {
    const group = await this.groupsRepository.findOne({
      where: { id },
    });

    if (!group) {
      throw new NotFoundException('Group not found');
    }

    return await this.groupsRepository.delete(id);
  }
}
