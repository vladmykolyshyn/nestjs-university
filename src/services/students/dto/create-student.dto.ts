import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";

export class CreateStudentDto {
  @ApiProperty({
    example: 'Ivan',
  })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({
    example: 'Ivanov',
  })
  @IsNotEmpty()
  @IsString()
  surname: string;

  @ApiProperty({
    example: 'ivan@gmail.com',
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty({
    example: 25,
  })
  @IsNotEmpty()
  @IsNumber()
  age: number;

  @ApiPropertyOptional({
    example: 'loyal',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  imagePath?: string | null;
}
