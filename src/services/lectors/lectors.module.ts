import { Module } from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { LectorsController } from '../../controllers/lectors/lectors.controller';
import { Lector } from './entities/lector.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Lector])],
  controllers: [LectorsController],
  providers: [LectorsService],
  exports: [LectorsService],
})
export class LectorsModule {}
