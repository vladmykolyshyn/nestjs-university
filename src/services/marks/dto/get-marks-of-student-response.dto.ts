import { ApiProperty } from '@nestjs/swagger';

export class GetMarksOfStudentResponseDto {

  @ApiProperty({ example: 'English' })
  courseName: string;

  @ApiProperty({ example: 95 })
  mark: number;

}
