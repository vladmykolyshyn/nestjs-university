import { ApiProperty } from '@nestjs/swagger';

export class GetCourseResponseDto {
  @ApiProperty({example: 1 })
  id: number;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  createdAt: Date;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  updatedAt: Date;

  @ApiProperty({example: 'English' })
  name: string;

  @ApiProperty({
    example: 'language',
  })
  description: string;

  @ApiProperty({example: 75 })
  hours: number;
}
