import { Module } from '@nestjs/common';
import { MarksService } from './marks.service';
import { MarksController } from '../../controllers/marks/marks.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import { Course } from '../courses/entities/course.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { Student } from '../students/entities/student.entity';
import { StudentsService } from '../students/students.service';
import { LectorsService } from '../lectors/lectors.service';
import { CoursesService } from '../courses/courses.service';

@Module({
  imports: [TypeOrmModule.forFeature([Mark, Student, Lector, Course])],
  controllers: [MarksController],
  providers: [MarksService, StudentsService, LectorsService, CoursesService],
})
export class MarksModule {}
