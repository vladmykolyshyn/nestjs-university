import { MigrationInterface, QueryRunner } from "typeorm";

export class RenameUserIdInTokenTable1693427315920 implements MigrationInterface {
    name = 'RenameUserIdInTokenTable1693427315920'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "tokens" DROP CONSTRAINT "FK_38d7436a04ad193463a87944153"`);
        await queryRunner.query(`ALTER TABLE "tokens" RENAME COLUMN "lector_id" TO "userId"`);
        await queryRunner.query(`ALTER TABLE "tokens" ADD CONSTRAINT "FK_d417e5d35f2434afc4bd48cb4d2" FOREIGN KEY ("userId") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "tokens" DROP CONSTRAINT "FK_d417e5d35f2434afc4bd48cb4d2"`);
        await queryRunner.query(`ALTER TABLE "tokens" RENAME COLUMN "userId" TO "lector_id"`);
        await queryRunner.query(`ALTER TABLE "tokens" ADD CONSTRAINT "FK_38d7436a04ad193463a87944153" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
