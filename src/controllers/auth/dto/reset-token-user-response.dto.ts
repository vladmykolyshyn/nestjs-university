import { ApiProperty } from '@nestjs/swagger';

export class ResetTokenResponseUserDto {
  @ApiProperty({ example: 1 })
  userId: number;
}
