import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from './app.service';
import { typeOrmAsyncConfig } from '../configs/database/typeorm-config';
import { ConfigModule } from '../configs/config.module';
import { AuthControllerModule } from '../controllers/auth/auth.controller.module';
import { AuthModule } from '../services/auth/auth.module';
import { ResetTokenModule } from '../services/reset-token/reset-token.module';
import { StudentsModule } from 'src/services/students/students.module';
import { MarksModule } from 'src/services/marks/marks.module';
import { LectorsModule } from 'src/services/lectors/lectors.module';
import { GroupsModule } from 'src/services/groups/groups.module';
import { CoursesModule } from 'src/services/courses/courses.module';
import { MailerModule } from '@nestjs-modules/mailer';
import dotenv from 'dotenv';

dotenv.config();



@Module({
  imports: [
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    ConfigModule,
    StudentsModule,
    MarksModule,
    LectorsModule,
    GroupsModule,
    CoursesModule,
    AuthControllerModule,
    AuthModule,
    ResetTokenModule,
    MailerModule.forRoot({
      transport: {
        host: process.env.EMAIL_HOST,
        port: Number(process.env.EMAIL_PORT),
        secure: false,
        requireTLS: true,
        tls: { ciphers: 'SSLv3' },
        auth: {
          user: process.env.EMAIL_ID,
          pass: process.env.EMAIL_PASS,
        },
      },
    }),
  ],
  providers: [AppService],
})
export class AppModule {}
