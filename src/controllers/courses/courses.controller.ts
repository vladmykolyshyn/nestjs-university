import {
  Controller,
  Get,
  Post,
  Body,
  HttpCode,
  UsePipes,
  ValidationPipe,
  Patch,
  Param,
  ParseIntPipe,
  HttpStatus,
  UseGuards,
  Delete,
} from '@nestjs/common';
import { CoursesService } from '../../services/courses/courses.service';
import { CreateCourseDto } from '../../services/courses/dto/create-course.dto';
import { AddLectorToCourseDto } from '../../services/courses/dto/add-lector-to-course.dto';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';
import { ApiSecurity, ApiBody, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { GetCourseResponseDto } from 'src/services/courses/dto/get-course-response.dto';
import { CreateCourseResponseDto } from 'src/services/courses/dto/create-course-response.dto';
import { UpdateCourseDto } from 'src/services/courses/dto/update-course.dto';
import { AddStudentToCourseDto } from 'src/services/courses/dto/add-student-to-course.dto';
import { GetCourseWithStudentResponseDto } from 'src/services/courses/dto/get-course-with-students-response.dto';

@Controller('courses')
@ApiTags('Courses')
@ApiSecurity('JWT-auth')
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @ApiOperation({ summary: 'Get all courses' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The list of courses',
    type: GetCourseResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getAllCourses() {
    return this.coursesService.getAllCourses();
  }

  @ApiOperation({ summary: 'Get all courses with students' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The list of courses with students',
    type: GetCourseWithStudentResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get('students')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getAllCoursesWithStudents() {
    return this.coursesService.getAllCoursesWithStudents();
  }

  @ApiOperation({ summary: 'Get course by id' })
  @ApiParam({
    name: 'id',
    description: 'Course id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The course by id',
    type: GetCourseResponseDto,
    isArray: false,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Course not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getCourseById(@Param('id', ParseIntPipe) id: number) {
    return this.coursesService.getCourseById(+id);
  }

  @ApiOperation({ summary: 'Add lector to course' })
  @ApiParam({
    name: 'id',
    description: 'Course id',
    required: true,
    type: Number,
  })
  @ApiBody({ type: AddLectorToCourseDto })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Lector is added',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Course or lector not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Patch(':id/lector')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  addLectorToCourse(
    @Param('id', ParseIntPipe) id: number,
    @Body() addLectorToCourseSchema: AddLectorToCourseDto,
  ) {
    return this.coursesService.addLectorToCourse(+id, addLectorToCourseSchema);
  }

  @ApiOperation({ summary: 'Add student to course' })
  @ApiParam({
    name: 'id',
    description: 'Course id',
    required: true,
    type: Number,
  })
  @ApiBody({ type: AddStudentToCourseDto })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Student is added',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Course or student not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Patch(':id/student')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  addStudentToCourse(
    @Param('id', ParseIntPipe) id: number,
    @Body() addStudentToCourseSchema: AddStudentToCourseDto,
  ) {
    return this.coursesService.addStudentToCourse(
      +id,
      addStudentToCourseSchema,
    );
  }

  @ApiOperation({ summary: 'Create course' })
  @ApiBody({ type: CreateCourseDto })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Return the course after creation',
    type: CreateCourseResponseDto,
    isArray: false,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed or course with this name already exist',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Post()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.CREATED)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  createCourse(@Body() createCourseDto: CreateCourseDto) {
    return this.coursesService.createCourse(createCourseDto);
  }

  @ApiOperation({ summary: 'Update course' })
  @ApiParam({
    name: 'id',
    description: 'Course id',
    required: true,
    type: Number,
  })
  @ApiBody({ type: UpdateCourseDto })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Course is updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Course not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Patch(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  updateStudentById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateCourseDto: UpdateCourseDto,
  ) {
    return this.coursesService.updateCourseById(+id, updateCourseDto);
  }

  @ApiOperation({ summary: 'Delete course by id' })
  @ApiParam({
    name: 'id',
    description: 'Course id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Course is deleted',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Course not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Delete(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  deleteCourseById(@Param('id', ParseIntPipe) id: number) {
    return this.coursesService.deleteCourseById(+id);
  }
}
