import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateStudentTable1692821239392 implements MigrationInterface {
    name = 'CreateStudentTable1692821239392'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "students" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying NOT NULL, "surname" character varying NOT NULL, "email" character varying NOT NULL, "age" numeric NOT NULL, "imagePath" character varying NOT NULL, "group_id" integer, CONSTRAINT "PK_7d7f07271ad4ce999880713f05e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "idx_student_name" ON "students" ("name") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."idx_student_name"`);
        await queryRunner.query(`DROP TABLE "students"`);
    }

}
