import { Module } from '@nestjs/common';
import { StudentsService } from './students.service';
import { StudentsController } from '../../controllers/students/students.controller';
import { Student } from './entities/student.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Group } from '../groups/entities/group.entity';
import { GroupsService } from '../groups/groups.service';

@Module({
  imports: [TypeOrmModule.forFeature([Student, Group])],
  controllers: [StudentsController],
  providers: [StudentsService, GroupsService],
})
export class StudentsModule {}
