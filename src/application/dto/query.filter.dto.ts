import { IsOptional, IsString } from 'class-validator';

export class QueryFilterDto {
  @IsString()
  @IsOptional()
  public sortField?: string;

  @IsString()
  @IsOptional()
  public sortOrder?: string = 'DESC';

  @IsString()
  @IsOptional()
  public name?: string; 

  @IsString()
  @IsOptional()
  public surname?: string; 

  @IsString()
  @IsOptional()
  public groupName?: string; 

  @IsString()
  @IsOptional()
  public courseName?: string;
}
