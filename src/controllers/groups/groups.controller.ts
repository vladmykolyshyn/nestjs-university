import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  ParseIntPipe,
  UsePipes,
  ValidationPipe,
  UseGuards,
  HttpStatus,
} from '@nestjs/common';
import { GroupsService } from '../../services/groups/groups.service';
import { CreateGroupDto } from '../../services/groups/dto/create-group.dto';
import { UpdateGroupDto } from '../../services/groups/dto/update-group.dto';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';
import { ApiSecurity, ApiBody, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { GetGroupResponseDto } from 'src/services/groups/dto/get-group-response.dto';
import { CreateGroupResponseDto } from 'src/services/groups/dto/create-group-response.dto';

@Controller('groups')
@ApiTags('Groups')
@ApiSecurity('JWT-auth')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @ApiOperation({ summary: 'Get all groups' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The list of groups',
    type: GetGroupResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getAllGroups() {
    return this.groupsService.getAllGroups();
  }

  @ApiOperation({ summary: 'Get group by id' })
  @ApiParam({
    name: 'id',
    description: 'Group id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The group by id',
    type: GetGroupResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Group not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getGroupById(@Param('id', ParseIntPipe) id: number) {
    return this.groupsService.getGroupById(+id);
  }

  @ApiOperation({ summary: 'Create group' })
  @ApiBody({ type: CreateGroupDto })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Return the group after creation',
    type: CreateGroupResponseDto,
    isArray: false,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed or group with this name already exist',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Post()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.CREATED)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  createGroup(@Body() createGroupDto: CreateGroupDto) {
    return this.groupsService.createGroup(createGroupDto);
  }

  @ApiOperation({ summary: 'Update group' })
  @ApiParam({
    name: 'id',
    description: 'Group id',
    required: true,
    type: Number,
  })
  @ApiBody({ type: UpdateGroupDto })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Group is updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student or group not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Patch(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  updateGroupById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateGroupDto: UpdateGroupDto,
  ) {
    return this.groupsService.updateGroupById(+id, updateGroupDto);
  }

  @ApiOperation({ summary: 'Delete group by id' })
  @ApiParam({
    name: 'id',
    description: 'Group id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Group is deleted',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Group not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Delete(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  deleteGroupById(@Param('id', ParseIntPipe) id: number) {
    return this.groupsService.deleteGroupById(+id);
  }
}
