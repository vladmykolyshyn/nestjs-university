import {ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
} from 'class-validator';

export class UpdateMarkDto {
  @ApiPropertyOptional({
    example: 95,
  })
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  mark: number;

  @ApiPropertyOptional({
    example: 2,
  })
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  student_id: number;

  @ApiPropertyOptional({
    example: 1,
  })
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  course_id: number;

  @ApiPropertyOptional({
    example: 3,
  })
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  lector_id: number;
}
