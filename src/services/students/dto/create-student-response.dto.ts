import { ApiProperty } from "@nestjs/swagger";

export class CreateStudentResponseDto {
  @ApiProperty({
    example: 'Ivan',
  })
  name: string;

  @ApiProperty({
    example: 'Ivanov',
  })
  surname: string;

  @ApiProperty({
    example: 'ivan@gmail.com',
  })
  email: string;

  @ApiProperty({
    example: 25,
  })
  age: number;

  @ApiProperty({
    example: 'loyal',
  })
  imagePath: string;

  @ApiProperty({
    nullable: true,
    example: 'null',
  })
  groupId: number;

  @ApiProperty({example: 1 })
  id: number;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  createdAt: Date;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  updatedAt: Date;
}
