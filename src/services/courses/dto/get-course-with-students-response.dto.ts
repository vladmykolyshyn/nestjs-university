import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Student } from 'src/services/students/entities/student.entity';

export class GetCourseWithStudentResponseDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  createdAt: Date;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  updatedAt: Date;

  @ApiProperty({ example: 'English' })
  name: string;

  @ApiProperty({
    example: 'language',
  })
  description: string;

  @ApiProperty({ example: 75 })
  hours: number;

  @ApiPropertyOptional({ example: 3 })
  studentCount?: number;

  @ApiPropertyOptional({ example: [] })
  students?: Student[];
}
