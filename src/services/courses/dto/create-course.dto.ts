import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class CreateCourseDto {
  @ApiProperty({
    example: 'English',
  })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({
    example: 'language',
  })
  @IsNotEmpty()
  @IsString()
  description: string;

  @ApiProperty({ example: 75 })
  @IsNotEmpty()
  @IsNumber()
  hours: number;
}
