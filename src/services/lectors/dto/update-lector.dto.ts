import {ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateLectorDto {
  @ApiPropertyOptional({
    example: 'Ivan',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  name?: string;

  @ApiPropertyOptional({
    example: 'Ivaniv',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  surname?: string;

  @ApiPropertyOptional({
    example: 'ivan@gmail.com',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsEmail()
  email?: string;

  @ApiPropertyOptional({
    example: 'ivan123',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  password?: string;
}
