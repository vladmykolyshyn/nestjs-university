import {
  BadRequestException,
  Injectable,
  Logger,
  NotFoundException,
  Optional,
} from '@nestjs/common';
import crypto from 'crypto';
import { ResetTokenInterface } from './interfaces/reset-token.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { Token } from './entities/token.entity';
import { DeleteResult, Repository } from 'typeorm';
import { LectorsService } from '../lectors/lectors.service';
import { MailerService } from '@nestjs-modules/mailer';
import dotenv from 'dotenv';

dotenv.config();



@Injectable()
export class ResetTokenService {
  private logger: Logger;
  constructor(
    @InjectRepository(Token)
    private readonly tokensRepository: Repository<Token>,
    @Optional()
    private lectorsService: LectorsService,
    private readonly mailerService: MailerService,
  ) {
    this.logger = new Logger(ResetTokenService.name);
  }

  public async generateResetToken(
    userId: number,
  ): Promise<ResetTokenInterface> {
    const existingUser = await this.tokensRepository.findOne({
      where: { userId },
    });
    if (existingUser) {
      throw new BadRequestException(`For this user email is already send.`);
    }

    const token = crypto.randomBytes(32).toString('hex');
    const resetPasswordObject = {
      userId,
      token,
    };
    
    const lector = await this.lectorsService.getLectorById(userId);
    const lectorEmail = lector.email;

    await this.sendResetTokenEmail(lectorEmail, token);

    return this.tokensRepository.save(resetPasswordObject);
  }

  private async sendResetTokenEmail(
    email: string,
    token: string,
  ): Promise<void> {
    const resetPasswordURL = `http://localhost:3000/reset-password?token=${token}`;

    await this.mailerService.sendMail({
      from: process.env.EMAIL_ID,
      to: email,
      subject: 'Reset Password',
      text: `
    You have requested to reset your password. Please follow the link below to reset it:
    ${resetPasswordURL}
    If you did not request this, please ignore this email.
  `,
    });
  }

  public async getResetToken(token: string): Promise<ResetTokenInterface> {
    const existingToken = await this.tokensRepository.findOne({
      where: { token },
    });
    if (!existingToken) {
      throw new NotFoundException(`Token not found `);
    }

    return existingToken;
  }

  async removeResetToken(token: string): Promise<DeleteResult> {
    const tokenFromUrl = await this.tokensRepository.findOne({
      where: { token },
    });

    if (!tokenFromUrl) {
      throw new NotFoundException('Token not found');
    }

    return await this.tokensRepository.delete(tokenFromUrl.id);
  }
  
}
