import { Entity, Column, ManyToMany, OneToMany } from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { Student } from '../../students/entities/student.entity';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  description: string;

  @Column({
    type: 'integer',
    nullable: false,
  })
  hours: number;

  @ManyToMany(() => Lector, (lector) => lector.courses, {
    cascade: true,
    eager: true,
  })
  lectors: Lector[];

  @ManyToMany(() => Student, (student) => student.courses, {
    cascade: true,
    eager: true,
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  students?: Student[];

  @OneToMany(() => Mark, (mark) => mark.course)
  marks: Mark[];
}
