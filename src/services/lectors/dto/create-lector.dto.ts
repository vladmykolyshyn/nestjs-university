import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateLectorDto {
  @ApiPropertyOptional({
    example: 'Roman',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  name?: string;

  @ApiPropertyOptional({
    example: 'Romanovych',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  surname?: string;

  @ApiProperty({
    example: 'ivan@gmail.com',
  })
  @IsNotEmpty()
  @IsString()
  email: string;

  @ApiProperty({
    example: 'ivan123',
  })
  @IsNotEmpty()
  @IsString()
  password: string;
}
