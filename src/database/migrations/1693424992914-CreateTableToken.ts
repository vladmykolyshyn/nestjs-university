import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTableToken1693424992914 implements MigrationInterface {
    name = 'CreateTableToken1693424992914'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "tokens" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lector_id" integer NOT NULL, "token" character varying NOT NULL, CONSTRAINT "REL_38d7436a04ad193463a8794415" UNIQUE ("lector_id"), CONSTRAINT "PK_3001e89ada36263dabf1fb6210a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "tokens" ADD CONSTRAINT "FK_38d7436a04ad193463a87944153" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "tokens" DROP CONSTRAINT "FK_38d7436a04ad193463a87944153"`);
        await queryRunner.query(`DROP TABLE "tokens"`);
    }

}
