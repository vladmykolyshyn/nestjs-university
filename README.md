# nestjs-university
- For full implementation use front-end of this project - https://gitlab.com/vladmykolyshyn/university-frontend
- Implemented jwt auth using Nest.js docs (for FE logic)
- Configured connection to db
- Added Swagger docs for auth endpoints - Open http://localhost:{APP_PORT}/docs
- Implemented logic with reset password 
- Logic for reset password - send POST http://localhost:3010/auth/reset-password-request and receive a token -> then use http://localhost:3010/auth/reset-password endpoint to change user password (token is passed to dto). 
For sending letter I use my mail and password from outlook mail. And I receive all letters with restoration's password link in mailinator.com - https://www.mailinator.com/
If you want use this service also you should create your users(lectors) with email ...@mailinator.com
- Implemented guard 
- Modify auth to use database instead of storing everything in memory. In our system lector should be an entity which we need to use for auth. It already has email and password fields 
- Password for lectors is stored in database using some hash algorithm

