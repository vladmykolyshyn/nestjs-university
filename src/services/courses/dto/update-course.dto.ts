import {ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateCourseDto {
  @ApiPropertyOptional({
    example: 'English',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiPropertyOptional({
    example: 'language',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  description: string;

  @ApiPropertyOptional({
    example: 75,
  })
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  hours: number;
}
