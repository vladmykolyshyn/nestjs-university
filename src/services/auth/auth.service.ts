import {
  BadRequestException,
  Injectable,
  Optional,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ResetTokenService } from '../reset-token/reset-token.service';
import { SignResponseDto } from '../../controllers/auth/dto/sign.response.dto';
import { ResetTokenInterface } from '../reset-token/interfaces/reset-token.interface';
import { LectorsService } from '../lectors/lectors.service';
import { CreateLectorDto } from '../lectors/dto/create-lector.dto';
import * as argon2 from 'argon2';
import { UpdateLectorDto } from '../lectors/dto/update-lector.dto';
import { ResetTokenUserInterface } from '../reset-token/interfaces/reset-token-user.interface';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private resetTokenService: ResetTokenService,
    @Optional()
    private lectorsService: LectorsService,
  ) {}

  public async signUp(
    username: string,
    pass: string,
  ): Promise<SignResponseDto> {
    const lectorData: CreateLectorDto = {
      email: username, 
      password: pass,
    };
    const user = await this.lectorsService.createLector(lectorData);
    const payload = { sub: user.id, username: user.email };
    return {
      accessToken: await this.jwtService.signAsync(payload),
      userId: user.id,
      username: user.email,
    };
  }

  public async resetPasswordRequest(
    username: string,
  ): Promise<ResetTokenInterface> {
    const user = await this.lectorsService.findOne(username);
    if (!user) {
      throw new BadRequestException(
        `Cannot generate token for reset password request  because user ${username} is not found`,
      );
    }
    const userId = user.id;
    return await this.resetTokenService.generateResetToken(userId);
  }

  public async resetPassword(
    token: string,
    newPassword: string,
  ): Promise<ResetTokenUserInterface> {
    const resetPasswordRequest = await this.resetTokenService.getResetToken(
      token,
    );
    if (!resetPasswordRequest) {
      throw new BadRequestException(
        `There is no request password request for user`,
      );
    } else {
      console.log(resetPasswordRequest);
    }

    const user = await this.lectorsService.getLectorById(
      resetPasswordRequest.userId,
    );
    if (!user) {
      throw new BadRequestException(`User is not found`);
    } else {
      console.log(user);
    }

    const updateDto: UpdateLectorDto = {
      password: newPassword,
    };
    console.log(updateDto);

    await this.lectorsService.updateLectorById(user.id, updateDto);
    await this.resetTokenService.removeResetToken(token);

    const result: ResetTokenUserInterface = {
      userId: resetPasswordRequest.userId,
    };

    console.log(result);
    return result;
  }

  public async signIn(
    username: string,
    pass: string,
  ): Promise<SignResponseDto> {
    const user = await this.lectorsService.findOne(username);
    const passwordIsMatch = await argon2.verify(user.password, pass);
    if (!passwordIsMatch) {
      throw new UnauthorizedException();
    }
    const payload = { sub: user.id, username: user.email };
    return {
      accessToken: await this.jwtService.signAsync(payload),
      userId: user.id,
      username: user.email,
    };
  }
}
