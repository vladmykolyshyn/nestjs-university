import { ApiProperty } from '@nestjs/swagger';
import { Course } from 'src/services/courses/entities/course.entity';
import { Group } from 'src/services/groups/entities/group.entity';

export class GetStudentCoursesGroupsResponseDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  createdAt: Date;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  updatedAt: Date;

  @ApiProperty({ example: 'Ivan' })
  name: string;

  @ApiProperty({ example: 'Ivanov' })
  surname: string;

  @ApiProperty({
    example: 'ivan@gmail.com',
  })
  email: string;

  @ApiProperty({ example: 21 })
  age: number;

  @ApiProperty({ example: 'light' })
  imagePath: string;

  @ApiProperty({
    example: 'Modern',
    nullable: true,
  })
  group: Group;

  @ApiProperty({
    example: [],
  })
  courses?: Course[];
}
