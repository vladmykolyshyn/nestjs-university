import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateLectorDto } from './dto/create-lector.dto';
import { Lector } from './entities/lector.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { GetLectorResponseDto } from './dto/get-lector-response.dto';
import { GetCourseResponseDto } from '../courses/dto/get-course-response.dto';
import * as argon2 from 'argon2';
import { UpdateLectorDto } from './dto/update-lector.dto';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private lectorsRepository: Repository<Lector>,
  ) {}

  async getAllLectors(): Promise<GetLectorResponseDto[]> {
    const lectors = await this.lectorsRepository
      .createQueryBuilder('lector')
      .getMany();
    return lectors;
  }

  async getLectorById(id: number): Promise<Lector> {
    const lector = await this.lectorsRepository
      .createQueryBuilder('lector')
      .leftJoinAndSelect('lector.courses', 'course')
      .where('lector.id = :id', { id })
      .getOne();

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return lector;
  }

  async findOne(username: string): Promise<Lector | null> {
    return this.lectorsRepository.findOne({
      where: {
        email: username,
      },
    });
  }

  async getCoursesTaughtByLector(id: number): Promise<GetCourseResponseDto[]> {
    const lector = await this.lectorsRepository
      .createQueryBuilder('lector')
      .leftJoinAndSelect('lector.courses', 'course')
      .where('lector.id = :id', { id })
      .getOne();

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return lector.courses;
  }

  async createLector(lectorCreateSchema: CreateLectorDto): Promise<Lector> {
    const { name, surname, email, password } = lectorCreateSchema;

    const lectorWithEmail = await this.lectorsRepository.findOne({
      where: {
        email: lectorCreateSchema.email,
      },
    });

    if (lectorWithEmail) {
      throw new BadRequestException('Lector with this email already exists');
    }

    return this.lectorsRepository.save({
      name,
      surname,
      email,
      password: await argon2.hash(password),
    });
  }

  async updateLectorById(
    id: number,
    lectorUpdateSchema: UpdateLectorDto,
  ): Promise<UpdateResult> {
    const lector = await this.lectorsRepository.findOne({
      where: { id },
    });

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    if (lectorUpdateSchema.password) {
      const hashedPassword = await argon2.hash(lectorUpdateSchema.password);

      lectorUpdateSchema.password = hashedPassword;
    }

    return await this.lectorsRepository.update(id, lectorUpdateSchema);
  }

  async deleteLectorById(id: number): Promise<DeleteResult> {
    const lector = await this.lectorsRepository.findOne({
      where: { id },
    });

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return await this.lectorsRepository.delete(id);
  }
}
