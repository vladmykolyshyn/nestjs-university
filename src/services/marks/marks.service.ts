import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { AddMarkForStudentDto } from './dto/create-mark.dto';
import { StudentsService } from '../students/students.service';
import { LectorsService } from '../lectors/lectors.service';
import { CoursesService } from '../courses/courses.service';
import { CreateMarkResponseDto } from './dto/create-mark-response.dto';
import { GetMarksOfStudentResponseDto } from './dto/get-marks-of-student-response.dto';
import { GetMarksOfCourseResponseDto } from './dto/get-marks-of-course-response.dto';
import { GetMarkResponseDto } from './dto/get-mark-response.dto';
import { UpdateMarkDto } from './dto/update-mark.dto';

@Injectable()
export class MarksService {
  constructor(
    @InjectRepository(Mark)
    private readonly marksRepository: Repository<Mark>,
    private readonly studentsRepository: StudentsService,
    private readonly lectorsRepository: LectorsService,
    private readonly coursesRepository: CoursesService,
  ) {}

  async getAllMarks(): Promise<GetMarkResponseDto[]> {
    const marks = await this.marksRepository
      .createQueryBuilder('mark')
      .getMany();
    return marks;
  }

  async getMarkById(id: number): Promise<GetMarkResponseDto> {
    const mark = await this.marksRepository
      .createQueryBuilder('mark')
      .where('mark.id = :id', { id })
      .getOne();

    if (!mark) {
      throw new NotFoundException('Mark not found');
    }

    return mark;
  }

  async addMarkForStudent(
    addMarkForStudentSchema: AddMarkForStudentDto,
  ): Promise<CreateMarkResponseDto> {
    const { student_id } = addMarkForStudentSchema;

    const student = await this.studentsRepository.getStudentById(student_id);
    if (!student) {
      throw new NotFoundException('Student not found');
    }

    const { lector_id } = addMarkForStudentSchema;

    const lector = await this.lectorsRepository.getLectorById(lector_id);
    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    const { course_id } = addMarkForStudentSchema;

    const course = await this.coursesRepository.getCourseById(course_id);
    if (!course) {
      throw new NotFoundException('Course not found');
    }

    const mark = await this.marksRepository.findOne({
      where: {
        mark: addMarkForStudentSchema.mark,
      },
    });
    if (mark) {
      throw new BadRequestException('This mark already exists');
    }

    return this.marksRepository.save(addMarkForStudentSchema);
  }

  async getMarksOfStudent(
    student_id: number,
  ): Promise<GetMarksOfStudentResponseDto[]> {
    const student = await this.studentsRepository.getStudentById(student_id);

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    const marks = await this.marksRepository
      .createQueryBuilder('mark')
      .select(['course.name', 'mark.mark'])
      .leftJoin('mark.course', 'course')
      .where('mark.student_id = :student_id', { student_id })
      .getMany();

    return marks.map((mark) => ({
      courseName: mark.course.name,
      mark: mark.mark,
    }));
  }

  async getMarksOfCourse(
    course_id: number,
  ): Promise<GetMarksOfCourseResponseDto[]> {
    const course = await this.coursesRepository.getCourseById(course_id);

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    const marks = await this.marksRepository
      .createQueryBuilder('mark')
      .select(['course.name', 'lector.name', 'student.name', 'mark.mark'])
      .leftJoin('mark.course', 'course')
      .leftJoin('mark.lector', 'lector')
      .leftJoin('mark.student', 'student')
      .where('mark.course_id = :course_id', { course_id })
      .getMany();

    return marks.map((mark) => ({
      courseName: mark.course.name,
      lectorName: mark.lector.name,
      studentName: mark.student.name,
      mark: mark.mark,
    }));
  }

  async updateMarkById(
    id: number,
    markUpdateSchema: UpdateMarkDto,
  ): Promise<UpdateResult> {
    const mark = await this.marksRepository.findOne({
      where: { id },
    });

    if (!mark) {
      throw new NotFoundException('Mark not found');
    }

    const student = await this.studentsRepository.getStudentById(
      markUpdateSchema.student_id,
    );
    if (!student) {
      throw new NotFoundException('Student not found');
    }

    const course = await this.coursesRepository.getCourseById(
      markUpdateSchema.course_id,
    );
    if (!course) {
      throw new NotFoundException('Course not found');
    }

    const lector = await this.lectorsRepository.getLectorById(
      markUpdateSchema.lector_id,
    );
    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return await this.marksRepository.update(id, markUpdateSchema);
  }

  async deleteMarkById(id: number): Promise<DeleteResult> {
    const mark = await this.marksRepository.findOne({
      where: { id },
    });

    if (!mark) {
      throw new NotFoundException('Mark not found');
    }

    return await this.marksRepository.delete(id);
  }
}
