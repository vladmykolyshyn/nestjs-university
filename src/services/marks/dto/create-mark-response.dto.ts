import { ApiProperty } from "@nestjs/swagger";

export class CreateMarkResponseDto {
  @ApiProperty({
    example: 95,
  })
  mark: number;

  @ApiProperty({
    example: 2,
  })
  student_id: number;

  @ApiProperty({
    example: 1,
  })
  course_id: number;

  @ApiProperty({
    example: 3,
  })
  lector_id: number;

  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  createdAt: Date;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  updatedAt: Date;
}
