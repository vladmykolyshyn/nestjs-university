import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { MarksService } from '../../services/marks/marks.service';
import { AddMarkForStudentDto } from '../../services/marks/dto/create-mark.dto';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';
import { ApiSecurity, ApiBody, ApiOperation, ApiQuery, ApiResponse, ApiTags, ApiParam } from '@nestjs/swagger';
import { CreateMarkResponseDto } from 'src/services/marks/dto/create-mark-response.dto';
import { GetMarksOfStudentResponseDto } from 'src/services/marks/dto/get-marks-of-student-response.dto';
import { GetMarksOfCourseResponseDto } from 'src/services/marks/dto/get-marks-of-course-response.dto';
import { GetMarkResponseDto } from 'src/services/marks/dto/get-mark-response.dto';
import { UpdateMarkDto } from 'src/services/marks/dto/update-mark.dto';

@Controller('marks')
@ApiTags('Marks')
@ApiSecurity('JWT-auth')
export class MarksController {
  constructor(private readonly marksService: MarksService) {}

  @ApiOperation({ summary: 'Get all marks' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The list of marks',
    type: GetMarkResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getAllMarks() {
    return this.marksService.getAllMarks();
  }

  @ApiOperation({ summary: 'Get mark by id' })
  @ApiParam({
    name: 'id',
    description: 'Mark id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The mark by id',
    type: GetMarkResponseDto,
    isArray: false,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Mark not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getLectorById(@Param('id', ParseIntPipe) id: number) {
    return this.marksService.getMarkById(+id);
  }

  @ApiOperation({ summary: 'Get all marks of student' })
  @ApiQuery({
    name: 'student_id',
    description: 'Id of student',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The list of marks of student',
    type: GetMarksOfStudentResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get('/student')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getMarksOfStudent(@Query('student_id', ParseIntPipe) student_id: number) {
    return this.marksService.getMarksOfStudent(+student_id);
  }

  @ApiOperation({ summary: 'Get all marks of course' })
  @ApiQuery({
    name: 'course_id',
    description: 'Id of course',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The list of marks of course',
    type: GetMarksOfCourseResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Course not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get('/course')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getMarksOfCourse(@Query('course_id', ParseIntPipe) course_id: number) {
    return this.marksService.getMarksOfCourse(+course_id);
  }

  @ApiOperation({ summary: 'Create mark' })
  @ApiBody({ type: AddMarkForStudentDto })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Return the mark after creation',
    type: CreateMarkResponseDto,
    isArray: false,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed or this mark already exist',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Course or student or lector not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Post()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.CREATED)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  addMarkForStudent(@Body() addMarkForStudentSchema: AddMarkForStudentDto) {
    return this.marksService.addMarkForStudent(addMarkForStudentSchema);
  }

  @ApiOperation({ summary: 'Update mark' })
  @ApiParam({
    name: 'id',
    description: 'Mark id',
    required: true,
    type: Number,
  })
  @ApiBody({ type: UpdateMarkDto })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Mark is updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Mark or student, or course, orelctor not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Patch(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  updateStudentById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateMarkDto: UpdateMarkDto,
  ) {
    return this.marksService.updateMarkById(+id, updateMarkDto);
  }

  @ApiOperation({ summary: 'Delete mark by id' })
  @ApiParam({
    name: 'id',
    description: 'Mark id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Mark is deleted',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Mark not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Delete(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  deleteMarkById(@Param('id', ParseIntPipe) id: number) {
    return this.marksService.deleteMarkById(+id);
  }
}
