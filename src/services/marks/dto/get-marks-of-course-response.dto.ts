import { ApiProperty } from '@nestjs/swagger';

export class GetMarksOfCourseResponseDto {
  @ApiProperty({ example: 'English' })
  courseName: string;

  @ApiProperty({ example: 'Ivaniv' })
  lectorName: string;

  @ApiProperty({ example: 'Andriy' })
  studentName: string;

  @ApiProperty({ example: 95 })
  mark: number;
}
