import { Module } from '@nestjs/common';
import { CoursesService } from './courses.service';
import { CoursesController } from '../../controllers/courses/courses.controller';
import { Course } from './entities/course.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lector } from '../lectors/entities/lector.entity';
import { LectorsService } from '../lectors/lectors.service';
import { Student } from '../students/entities/student.entity';
import { StudentsService } from '../students/students.service';

@Module({
  imports: [TypeOrmModule.forFeature([Course, Lector, Student])],
  controllers: [CoursesController],
  providers: [CoursesService, LectorsService, StudentsService],
})
export class CoursesModule {}
