import {ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateStudentDto {
  @ApiPropertyOptional({
    example: 'Ivan',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiPropertyOptional({
    example: 'Ivanov',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  surname: string;

  @ApiPropertyOptional({
    example: 'ivan@gmail.com',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiPropertyOptional({
    example: 25,
  })
  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  age: number;

  @ApiPropertyOptional({
    example: 'loyal',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  imagePath: string;

  @ApiPropertyOptional({
    example: '1',
  })
  @IsOptional()
  @IsNumber()
  groupId: number;
}
