import {
  BadRequestException,
  Injectable,
  NotFoundException,
  Optional,
} from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Student } from './entities/student.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { GroupsService } from '../groups/groups.service';
import { CreateStudentResponseDto } from './dto/create-student-response.dto';
import { AddGroupToStudentDto } from './dto/add-group-to-student.dto';
import { GetStudentResponseDto } from './dto/get-student-response.dto';
import { GetStudentCoursesGroupsResponseDto } from './dto/get-student-courses-groups-response.dto';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentsRepository: Repository<Student>,
    @Optional()
    private readonly groupsRepository: GroupsService,
  ) {}

  async getAllStudents(): Promise<GetStudentResponseDto[]> {
    const students = await this.studentsRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.createdAt as "createdAt"',
        'student.updatedAt as "updatedAt"',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
        'student.imagePath as imagePath',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .getRawMany();
    return students;
  }

  async getAllStudentsWithGroupsAndCourses(queryFilter: QueryFilterDto): Promise<
    GetStudentCoursesGroupsResponseDto[]
  > {
    let query = this.studentsRepository
      .createQueryBuilder('student')
      .leftJoinAndSelect('student.group', 'group')
      .leftJoinAndSelect('student.courses', 'courses');

    if (queryFilter.sortField && (queryFilter.sortOrder === 'ASC' || queryFilter.sortOrder === 'DESC')) {
      query = query.orderBy(`student.${queryFilter.sortField}`, queryFilter.sortOrder as 'ASC' | 'DESC');
    }

    if (queryFilter.name) {
    query = query.andWhere('LOWER(student.name) LIKE LOWER(:name)', { name: `%${queryFilter.name}%` });
    }

    if (queryFilter.surname) {
      query = query.andWhere('LOWER(student.surname) LIKE LOWER(:surname)', { surname: `%${queryFilter.surname}%` });
    }

    if (queryFilter.groupName) {
      query = query.andWhere('LOWER(group.name) LIKE LOWER(:groupName)', { groupName: `%${queryFilter.groupName}%` });
    }

    if (queryFilter.courseName) {
      query = query.andWhere('LOWER(courses.name) LIKE LOWER(:courseName)', { courseName: `%${queryFilter.courseName}%` });
    }

    const students = await query.getMany();
    return students;
  }

  async getStudentsByName(name: string): Promise<GetStudentResponseDto[]> {
    const students = await this.studentsRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.createdAt as "createdAt"',
        'student.updatedAt as "updatedAt"',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
        'student.imagePath as imagePath',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .where('student.name = :name', { name })
      .getRawMany();

    if (students.length === 0) {
      throw new NotFoundException('No students found with the specified name');
    }

    return students;
  }

  async findOne(id: number): Promise<Student | null> {
    return this.studentsRepository.findOne({
      where: {
        id,
      },
    });
  }

  async getStudentById(id: number): Promise<GetStudentResponseDto> {
    const student = await this.studentsRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.createdAt as "createdAt"',
        'student.updatedAt as "updatedAt"',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
        'student.imagePath as imagePath',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .where('student.id = :id', { id })
      .getRawOne();

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    return student;
  }

  async createStudent(
    studentCreateSchema: CreateStudentDto,
  ): Promise<CreateStudentResponseDto> {
    const studentWithEmail = await this.studentsRepository.findOne({
      where: {
        email: studentCreateSchema.email,
      },
    });

    if (studentWithEmail) {
      throw new BadRequestException('Student with this email already exists');
    }

    return this.studentsRepository.save(studentCreateSchema);
  }

  async updateStudentById(
    id: number,
    studentUpdateSchema: UpdateStudentDto,
  ): Promise<UpdateResult> {
    const student = await this.studentsRepository.findOne({
      where: { id },
    });

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    if (studentUpdateSchema.groupId) {
      const group = await this.groupsRepository.getGroupById(
        studentUpdateSchema.groupId,
      );

      if (!group) {
        throw new NotFoundException('Group not found');
      }
    }
    return await this.studentsRepository.update(id, studentUpdateSchema);
  }

  async addGroupToStudent(
    id: number,
    studentGroupSchema: AddGroupToStudentDto,
  ): Promise<UpdateResult> {
    const { groupId } = studentGroupSchema;

    const group = await this.groupsRepository.getGroupById(groupId);
    if (!group) {
      throw new NotFoundException('Group not found');
    }

    const addedGroup = await this.studentsRepository.update(
      id,
      studentGroupSchema,
    );

    if (!addedGroup.affected) {
      throw new NotFoundException('Student not found');
    }

    return addedGroup;
  }

  async deleteStudentById(id: number): Promise<DeleteResult> {
    const student = await this.studentsRepository.findOne({
      where: { id },
    });

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    return await this.studentsRepository.delete(id);
  }
}
