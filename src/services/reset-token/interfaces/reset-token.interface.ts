export interface ResetTokenInterface {
  userId: number;
  token: string;
  id: number;
  createdAt: Date;
  updatedAt: Date;
}
