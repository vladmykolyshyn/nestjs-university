import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString} from 'class-validator';

export class UpdateGroupDto {
  @ApiPropertyOptional({
    example: 'Ivan',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  name: string;
}
