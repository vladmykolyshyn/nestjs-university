import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  HttpCode,
  UsePipes,
  ValidationPipe,
  ParseIntPipe,
  HttpStatus,
  UseGuards,
  Patch,
  Delete,
} from '@nestjs/common';
import { LectorsService } from '../../services/lectors/lectors.service';
import { CreateLectorDto } from '../../services/lectors/dto/create-lector.dto';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';
import {
  ApiSecurity,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetLectorResponseDto } from 'src/services/lectors/dto/get-lector-response.dto';
import { GetLectorByIdResponseDto } from 'src/services/lectors/dto/get-lector-by-id-response.dto';
import { GetCourseResponseDto } from 'src/services/courses/dto/get-course-response.dto';
import { CreateLectorResponseDto } from 'src/services/lectors/dto/create-lector-response.dto';
import { UpdateLectorDto } from 'src/services/lectors/dto/update-lector.dto';

@Controller('lectors')
@ApiTags('Lectors')
@ApiSecurity('JWT-auth')
export class LectorsController {
  constructor(private readonly lectorsService: LectorsService) {}

  @ApiOperation({ summary: 'Get all lectors' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The list of lectors',
    type: GetLectorResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getAllLectors() {
    return this.lectorsService.getAllLectors();
  }

  @ApiOperation({ summary: 'Get lector by id' })
  @ApiParam({
    name: 'id',
    description: 'Lector id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The lector by id',
    type: GetLectorByIdResponseDto,
    isArray: false,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getLectorById(@Param('id', ParseIntPipe) id: number) {
    return this.lectorsService.getLectorById(+id);
  }

  @ApiOperation({ summary: 'Get courses taught by lector' })
  @ApiParam({
    name: 'id',
    description: 'Lector id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The courses taught by lector',
    type: GetCourseResponseDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Get(':id/courses')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  getCoursesTaughtByLector(@Param('id', ParseIntPipe) id: number) {
    return this.lectorsService.getCoursesTaughtByLector(+id);
  }

  @ApiOperation({ summary: 'Create lector' })
  @ApiBody({ type: CreateLectorDto })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Return the lector after creation',
    type: CreateLectorResponseDto,
    isArray: false,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed or lector with this email already exist',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Post()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.CREATED)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  createLector(@Body() createLectorDto: CreateLectorDto) {
    return this.lectorsService.createLector(createLectorDto);
  }

  @ApiOperation({ summary: 'Update lector' })
  @ApiParam({
    name: 'id',
    description: 'Lector id',
    required: true,
    type: Number,
  })
  @ApiBody({ type: UpdateLectorDto })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Lector is updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Patch(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
    }),
  )
  updateLectorById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateLectorDto: UpdateLectorDto,
  ) {
    return this.lectorsService.updateLectorById(+id, updateLectorDto);
  }

  @ApiOperation({ summary: 'Delete lector by id' })
  @ApiParam({
    name: 'id',
    description: 'Lector id',
    required: true,
    type: Number,
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Lector is deleted',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal server error',
  })
  @Delete(':id')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  deleteStudentById(@Param('id', ParseIntPipe) id: number) {
    return this.lectorsService.deleteLectorById(+id);
  }
}
