import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { Course } from './entities/course.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { LectorsService } from '../lectors/lectors.service';
import { AddLectorToCourseDto } from './dto/add-lector-to-course.dto';
import { GetCourseResponseDto } from './dto/get-course-response.dto';
import { CreateCourseResponseDto } from './dto/create-course-response.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { StudentsService } from '../students/students.service';
import { AddStudentToCourseDto } from './dto/add-student-to-course.dto';
import { GetCourseWithStudentResponseDto } from './dto/get-course-with-students-response.dto';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly coursesRepository: Repository<Course>,
    private readonly lectorsRepository: LectorsService,
    private readonly studentsRepository: StudentsService,
  ) {}

  async getAllCourses(): Promise<GetCourseResponseDto[]> {
    const courses = await this.coursesRepository
      .createQueryBuilder('course')
      .getMany();
    return courses;
  }

  async getAllCoursesWithStudents(): Promise<GetCourseWithStudentResponseDto[]> {
    const courses = await this.coursesRepository
      .createQueryBuilder('course')
      .leftJoinAndSelect('course.students', 'student')
      .loadRelationCountAndMap('course.studentCount', 'course.students') // Завантажити кількість студентів в кожному курсі
      .getMany();
    return courses;
  }

  async getCourseById(id: number): Promise<GetCourseResponseDto> {
    const course = await this.coursesRepository
      .createQueryBuilder('course')
      .where('course.id = :id', { id })
      .getOne();

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    return course;
  }

  async createCourse(
    courseCreateSchema: CreateCourseDto,
  ): Promise<CreateCourseResponseDto> {
    const courseWithName = await this.coursesRepository.findOne({
      where: {
        name: courseCreateSchema.name,
      },
    });

    if (courseWithName) {
      throw new BadRequestException('Course with this name already exists');
    }

    return this.coursesRepository.save(courseCreateSchema);
  }

  async addLectorToCourse(
    courseId: number,
    addLectorToCourseSchema: AddLectorToCourseDto,
  ): Promise<Course> {
    const course = await this.coursesRepository.findOne({
      where: { id: courseId },
    });
    if (!course) {
      throw new NotFoundException('Course not found');
    }

    const lector = await this.lectorsRepository.getLectorById(
      addLectorToCourseSchema.lectorId,
    );
    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    course.lectors = course.lectors || [];
    course.lectors.push(lector);
    return this.coursesRepository.save(course);
  }

  async addStudentToCourse(
    courseId: number,
    addStudentToCourseSchema: AddStudentToCourseDto,
  ): Promise<Course> {
    const course = await this.coursesRepository.findOne({
      where: { id: courseId },
    });
    if (!course) {
      throw new NotFoundException('Course not found');
    }

    const student = await this.studentsRepository.findOne(
      addStudentToCourseSchema.studentId,
    );
    if (!student) {
      throw new NotFoundException('Student not found');
    }

    course.students = course.students || [];
    course.students.push(student);
    return this.coursesRepository.save(course);
  }

  async updateCourseById(
    id: number,
    courseUpdateSchema: UpdateCourseDto,
  ): Promise<UpdateResult> {
    const course = await this.coursesRepository.findOne({
      where: { id },
    });

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    return await this.coursesRepository.update(id, courseUpdateSchema);
  }

  async deleteCourseById(id: number): Promise<DeleteResult> {
    const course = await this.coursesRepository.findOne({
      where: { id },
    });

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    return await this.coursesRepository.delete(id);
  }
}
