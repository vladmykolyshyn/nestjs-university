import { ApiProperty } from '@nestjs/swagger';

export class GetLectorResponseDto {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  createdAt: Date;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  updatedAt: Date;

  @ApiProperty({ example: 'Ivan' })
  name: string;

  @ApiProperty({ example: 'Ivaniv' })
  surname: string;

  @ApiProperty({
    example: 'ivan@gmail.com',
  })
  email: string;

  @ApiProperty({ example: 'pass_123' })
  password: string;
}
