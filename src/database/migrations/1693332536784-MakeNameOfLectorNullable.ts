import { MigrationInterface, QueryRunner } from "typeorm";

export class MakeNameOfLectorNullable1693332536784 implements MigrationInterface {
    name = 'MakeNameOfLectorNullable1693332536784'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lectors" ALTER COLUMN "name" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lectors" ALTER COLUMN "name" SET NOT NULL`);
    }

}
