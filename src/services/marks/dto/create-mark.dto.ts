import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber } from "class-validator";

export class AddMarkForStudentDto {
  @ApiProperty({
    example: 95,
  })
  @IsNotEmpty()
  @IsNumber()
  mark: number;

  @ApiProperty({
    example: 2,
  })
  @IsNotEmpty()
  @IsNumber()
  student_id: number;

  @ApiProperty({
    example: 1,
  })
  @IsNotEmpty()
  @IsNumber()
  course_id: number;

  @ApiProperty({
    example: 3,
  })
  @IsNotEmpty()
  @IsNumber()
  lector_id: number;
}
