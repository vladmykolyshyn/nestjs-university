import { MigrationInterface, QueryRunner } from "typeorm";

export class StudentTableMakeImagePathNullAble1694190688594 implements MigrationInterface {
    name = 'StudentTableMakeImagePathNullAble1694190688594'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "students" ALTER COLUMN "imagePath" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "students" ALTER COLUMN "imagePath" SET NOT NULL`);
    }

}
