import { ApiProperty } from "@nestjs/swagger";

export class CreateGroupResponseDto {
  @ApiProperty({
    example: 'Technology',
  })
  name: string;

  @ApiProperty({example: 1 })
  id: number;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  createdAt: Date;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  updatedAt: Date;
}
