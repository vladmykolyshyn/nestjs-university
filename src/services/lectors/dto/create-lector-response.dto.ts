import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export class CreateLectorResponseDto {
  @ApiPropertyOptional({
    example: 'Ivan',
  })
  name: string;

  @ApiPropertyOptional({
    example: 'Ivaniv',
  })
  surname: string;

  @ApiProperty({
    example: 'ivan@gmail.com',
  })
  email: string;

  @ApiProperty({
    example: 'ivan123',
  })
  password: string;

  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  createdAt: Date;

  @ApiProperty({
    example: new Date('2023-08-26T04:50:32.949Z'),
  })
  updatedAt: Date;
}
